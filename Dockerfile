FROM node:18-slim
WORKDIR /src
COPY . .
RUN npm install
CMD node