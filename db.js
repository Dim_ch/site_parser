import pg from 'pg';

const { Pool } = pg;
 
const pool = new Pool()

export async function addHtmlData(parent_id = null, page) {
  return await pool.query(`
    INSERT INTO site (parent_id, html, title, url)
    VALUES ($1, $2, $3, $4) RETURNING id
  `, [
    parent_id,
    page.html,
    page.title,
    page.url
  ]);
}

export async function createTableIfNotExists()  {
  await pool.query(`
    CREATE TABLE IF NOT EXISTS site (
      id SERIAL PRIMARY KEY,
      parent_id INT NULL,
      html TEXT,
      title VARCHAR(200) NULL,
      url VARCHAR(2048),
      CONSTRAINT FK_parent_site
      FOREIGN KEY (parent_id)
      REFERENCES site (id) ON DELETE CASCADE
    )
  `);
}

export async function isExistData(url = "") {
  const res = await pool.query(`
  SELECT id FROM site WHERE url LIKE $1 and parent_id is NULL
  `, [url]);
  return res.rowCount;
}

export async function deleteData(url) {
  return await pool.query(`
  DELETE FROM site WHERE url LIKE $1 AND parent_id is NULL
  `, [url]);
}

export async function getData(url = "", limit = 1) {
  return await pool.query(`
  WITH RECURSIVE site_tree(id, url, title) AS (
    SELECT id, url, title
    FROM site
    WHERE url LIKE $1 AND parent_id IS NULL

    UNION ALL
    SELECT s.id, s.url, s.title
    FROM site s, site_tree st
    WHERE s.parent_id = st.id
  )
  SELECT url, title FROM site_tree
  LIMIT $2
  `, [url, limit]);
}

export async function close() {
  await pool.end();
}
