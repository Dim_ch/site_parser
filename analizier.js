import axios from 'axios';
import * as db from './db.js';
    
function getHtmlTitle(str = "") {
    const reg = /<title[^<>]*>(?<title>.+)<\/title>/mi;
    return str.match(reg)?.groups.title || '';
}
    
function isHtml(contentType = "") {
    return contentType.includes('text/html');
}

function isAnchorLink(url = "") {
    return url.trim().startsWith("#");
}

function getUrlFromHtml(str = "") {
    const reg = /<title[^<>]*>(?<title>.+)<\/title>|href="(?<url>[^"]+)"/gmi;
    const urls = [];

    for (let result of str.matchAll(reg)) {
        if (result && result.groups.url && !isAnchorLink(result.groups.url)) {
            urls.push(result.groups.url);
        }
    }

    return urls;
}

function getErrorUrlMessage(url) {
    return `An error occurred while processing the url(${url})`;
}

export async function analizeSite(url = null, depth = 0, parentId = null) {
    if (depth < 0 || depth > 2 || !url) {
        return;
    }

    try {
        let urls = [];
        let parent_id = null;

        {
            const request = await axios.get(url, {
                timeout: 5000,
            });
    
            if (!isHtml(request.headers['content-type'])) {
                return;
            }
    
            const res = await db.addHtmlData(
                parentId,
                {
                    html: request.data,
                    title: getHtmlTitle(request.data),
                    url: url.toString(),
                }
            );
    
            if (res.rowCount === 0) {
                throw new Error('Row count after query is 0');
            }
    
            if (depth !== 0) {
                parent_id = res.rows[0].id;
                urls = getUrlFromHtml(request.data);
            }
        }

        for (let childUrl of urls) {
            await analizeSite(
                new URL(childUrl, url),
                depth - 1,
                parent_id
            );
        }
    } catch(error) {
        console.error(`${getErrorUrlMessage(url)}. Error: ${error.message}`)
    }
}