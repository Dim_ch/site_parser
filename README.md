# Запуск проекта
* склонировать проект:
```
git clone https://gitlab.com/Dim_ch/site_parser.git
```
* скопировать файл .env.example в файл .env
* настроить переменные окружения в .env файле
* запустить контейнер:
```
docker-compose --env-file .env up -d
```
* выполнить команду:
```
docker exec -it site_parser_node bash
```
* Запросить данные по url и глубине
```
node app load <url> --depth <глубина>
```
* Запросить данные из хранилища по url и количеству полученных страниц:
```
node app get <url> -n <количество прогруженных страниц>
```
* выйти из контейнера
```
exit
```
* остановить контейнер