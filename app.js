import process from 'node:process';
import * as db from './db.js';
import readlinePromises from 'node:readline/promises';
import { analizeSite } from './analizier.js';
import * as dotenv from 'dotenv'

dotenv.config()

async function printData(data) {
    for (let i = 0; i < data.rowCount; i++) {
        console.log(`${data.rows[i].url}: ${data.rows[i].title}`);
    }
}

function printResourceUsage() {
    console.log(`All memory usage: ${process.memoryUsage.rss() / 1024 / 1024} Mb`);
    console.log(`time: ${process.uptime()} sec`)
}

async function handleActionGet(url, n) {
    if (await db.isExistData(url)) {
        await printData(await db.getData(url, n));
    } else {
        console.log(`The row with url: ${url} does not exist in table`);
    }
}

async function handleActionLoad(url, depth) {
    if (await db.isExistData(url)) {
        const rl = readlinePromises.createInterface({
            input: process.stdin,
            output: process.stdout,
        });

        const answer = await rl.question("Such a row already exists in the table. Enter 'y|yes' to delete and continue: ");

        rl.close();

        if (answer.match(/^y(es)?$/i)) {
            await db.deleteData(url);
        } else {
            return;
        }
    }

    await analizeSite(new URL(url), depth, null);
}

async function run() {
    if (process.argv.length < 6) {
        console.log('Too few arguments');
        return;
    }
    
    if (!['load', 'get'].includes(process.argv[2])) {
        console.log("First parametr must be 'load' or 'get'");
        return;
    }

    const action = process.argv[2];
    const url = process.argv[3];
    const key = process.argv[4];
    const valueKey = process.argv[5];

    if (action === 'load' && key !== '--depth') {
        console.log("Third argument for 'load' must be '--depth'");
        return;
    } else if (action === 'get' && key !== '-n') {
        console.log("Third argument for 'get' must be '-n'");
        return;
    }
    
    try {
        await db.createTableIfNotExists();

        if (action === 'load') {
            await handleActionLoad(new URL(url).toString(), valueKey);
        } else {
            await handleActionGet(new URL(url).toString(), valueKey);
        }

        printResourceUsage();
    } catch (error) {
        console.error(error)
    } finally {
        db.close();
    }
}

run();